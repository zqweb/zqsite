#!/bin/bash

# config git public keys
cd /home/git
mkdir .ssh && chmod 700 .ssh
touch .ssh/authorized_keys && chmod 600 .ssh/authorized_keys
cat /zqSite/gitkey.pub >> /home/git/.ssh/authorized_keys

# init git repo
cd /home/git
mkdir -p gitsrv/zqSite.git
cd gitsrv/zqSite.git
git init --bare

# setup git hooks (post-receive)
cd /home/git/gitsrv/zqSite.git
cp /zqSite/githooks/post-receive ./hooks
chmod 755 -R hooks

# clone repo to /var/www
cd /var/www
rm -rf html
git clone /home/git/gitsrv/zqSite.git .
chmod 755 -R /var/www
chown -R git /var/www

# change dir owner
chown -R git /home/git

# start ssh for git
systemctl enable ssh
/etc/init.d/ssh start

# start web server
/etc/init.d/apache2 restart

# keep docker running
tail -f /dev/null
